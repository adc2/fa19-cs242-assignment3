#Manual Test Plan for Assignment 3

## Landing Page

1. Make sure page renders properly
2. Make sure user is able to type in text box
3. Make sure 'View' button triggers events that lead to the Profile page for the github username entered
4. Test for different github usernames
5. Test the loading spinner such that it starts when the user clicks on 'View'

## Profile Page

1. Ensure the proper name is being populated on the Profile page
2. Ensure the proper github username is being populated on the Profile page
3. Ensure the proper email is being populated on the Profile page
4. Ensure the proper bio is being populated on the Profile page
5. Ensure the proper avatar is being loaded on the Profile page
6. Ensure the proper number of followers is being populated on the Profile page
7. Ensure the proper number of people following this user is being populated on the Profile page
8. Ensure the proper number of public repositories is being populated on the Profile page
9. Check and make sure clicking on followers triggers an event that loads the followers page
10. Check and make sure clicking on following triggers an event that loads the following page
11. Check and make sure clicking on repositories count triggers an event that loads the repositories page
12. Make sure clicking the back button goes back to the Landing Page
13. Ensure the proper create date is being loaded on the Profile page
14. Ensure the correct website is loaded on to the profile page
15. Make sure the website is a clickable link and navigates to the given website

## Repositories Page

1. Make sure page renders properly
2. Make sure repositories for the correct github username is being populated
3. Make sure each repository name is a hyperlink to the github site for that repo
4. Make sure the correct descriptions for each repository are listed
5. Make sure the correct owner for each repository are listed
6. Make sure clicking the back button goes back to the Profile page

## Following Page

1. Make sure page renders properly
2. Make sure the following page lists all of the accounts the given user account is following 

## Followers Page

1. Make sure page renders properly
2. Make sure the followers page lists all of the accounts that follow the given user account