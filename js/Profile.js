'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Button,
    ActivityIndicator,
    ScrollView,
    Image,
} from 'react-native';

type Props = {};
export default class Profile extends Component<Props> {
    static navigationOptions = {
        title: 'Profile',
    };

    state = {
        githubUsername: '',
        info: {},
    }

    /*
        fetches the user information json and returns it
        for parsing
     */
    _getUserInfo = (username) => {
        username = username.toLowerCase().trim();
        const url = `https://api.github.com/users/${username}`;
        return fetch(url).then((res) => res.json());
    }

    /*
        retrieves the userinfo and sets the the info param
        in "state" to contain this new data, so we don't
        repeatedly make API calls
     */
    _handleSubmit = (githubUsername) => {
        this._getUserInfo(githubUsername)
            .then((res) => {
            this.setState({info: res});
        });
    }

    /*
        parses the relevant data and displays it on the
        screen
     */
    _renderProfile = (githubUsername) => {
        this._handleSubmit(githubUsername);
        console.log(this.state.info)
        return (
            <ScrollView>{
                <View>
                    //populate all of the user info with Text Views
                    <Text>Github Username: {this.state.info.login}</Text>
                    <Text>Bio: {this.state.info.bio}</Text>
                    <Text>Website: {this.state.info.html_url}</Text>
                    <Text>Email: {this.state.info.email}</Text>
                    <Text>Public Repos Count: {this.state.info.email}</Text>
                    <Text>Followers Count: {this.state.info.email}</Text>
                    <Text>Following Count: {this.state.info.email}</Text>
                    </View>
            }</ScrollView>
        )
    }

    //render the full view by calling the above method
    render() {
        //use the github username passed in from LandingPage to retrieve data
        const { params } = this.props.navigation.state;
        return (
            <View style={styles.container}>
                {this._renderProfile(params.githubUsername)}
            </View>
        );
    }
}

//style sheet for various buttons, views, etc.
const styles = StyleSheet.create({
    description: {
        marginBottom: 20,
        fontSize: 18,
        textAlign: 'center',
        color: '#656565'
    },
    container: {
        padding: 30,
        marginTop: 65,
        alignItems: 'center'
    },
});