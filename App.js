/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
'use strict';

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {
  createAppContainer
} from 'react-navigation';

import {
  createStackNavigator
} from 'react-navigation-stack';

import RepositoriesList from './js/RepositoriesList';
import FollowingPage from './js/FollowingPage';
import FollowersPage from './js/FollowersPage';
import Profile from './js/Profile';
import LandingPage from './js/LandingPage';

global.GithubUsername = '';

//central navigator that allows for easy transitions between layouts
const MainNavigator = createStackNavigator({
  LandingPage: { screen: LandingPage },
  Profile: { screen: Profile },
  RepositoriesList: { screen: RepositoriesList },
  FollowingPage: { screen: FollowingPage },
  FollowersPage: { screen: FollowersPage },
});

const App = createAppContainer(MainNavigator);
export default App;
