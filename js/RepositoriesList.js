'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Button,
    Linking,
    ActivityIndicator,
    ScrollView,
    Image,
} from 'react-native';

type Props = {};
export default class RepositoriesList extends Component<Props> {
    static navigationOptions = {
        title: 'Repositories List',
    };

    state = {
        githubUsername: '',
        repos: [],
    }

    /*
    make the HTTP GET request to retrieve the JSON data for the repos
     */
    _getUserRepos = (username) => {
        username = username.toLowerCase().trim();
        const url = `https://api.github.com/users/${username}/repos`;
        return fetch(url).then((res) => res.json());
    }

    /*
    once we retrieve the JSON data, store into main memory so we don't have to GET it as frequently
     */
    _handleSubmit = (githubUsername) => {
        this._getUserRepos(githubUsername)
            .then((res) => {
            this.setState({repos: res});
        });
    }

    /*
    parse the JSON into a list of repositories, along with their relevant links and descriptions
     */
    _renderRepos = (githubUsername) => {
        this._handleSubmit(githubUsername);
        console.log(this.state.repos)
        return (
            <ScrollView>{
                this.state.repos.map((repo, i) => {
                    return (
                        <View key={i}>
                            //make the repository name a clickable link to github.com
                            <Text onPress={() => Linking.openURL(repo.html_url)}>
                                Repository Name: {JSON.stringify(repo.name)}
                            </Text>

                            //print out the owner and description
                            <Text>Owner: {JSON.stringify(repo.owner.login)}</Text>
                            <Text>Description: {JSON.stringify(repo.description)}</Text>
                            <Text/>
                        </View>
                    )
                })
            }</ScrollView>
        )
    }

    render() {
        const { params } = this.props.navigation.state;
        return (
            <View style={styles.container}>
                //use the githubUsername from the previous page to populate this page
                {this._renderRepos(params.githubUsername)}
            </View>
        );
    }
}

//style sheet for the repositorieslist view
const styles = StyleSheet.create({
    description: {
        marginBottom: 20,
        fontSize: 18,
        textAlign: 'center',
        color: '#656565'
    },
    container: {
        padding: 30,
        marginTop: 65,
        alignItems: 'center'
    },
});