import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Dimensions,
    TouchableOpacity,
    Alert,
    Button,
    ActivityIndicator,
    Image,
    ScrollView,
} from 'react-native';

const screenWidth = Dimensions.get('window').width;

export default class LandingPage extends Component<Props> {
    static navigationOptions = {
        title: 'Landing Page',
    };

    constructor(props) {
        super(props);
        this.state = {
            githubUsername: '',
            isLoading: false,
        };
    }

    /*
        when the user hits the view button, this function will trigger a
        series of calls that will eventually end up at the Profile page
     */
    _handleSearch = (evt) => {
        this.setState({ isLoading: true });
        console.log(evt.nativeEvent.text);
        console.log(this.state.githubUsername)
        this.props.navigation.navigate('Profile', {githubUsername: this.state.githubUsername});
    }

    //as the user is typing, store the updated value into main memory
    _onSearchTextChanged = (event) => {
        this.setState({ githubUsername: event.nativeEvent.text });
    };

    render() {
        //add a spinner to show when pages are loading from this page
        const spinner = this.state.isLoading ? <ActivityIndicator size='large'/> : null;
        return (
            //ask user for the github username they want to search for
            <View style={styles.container}>
                <Text style={styles.description}>
                    Enter Your Github Username!
                </Text>
                <View style={styles.flowRight}>
                    <TextInput
                        //load the current value of the text box into main memory
                        underlineColorAndroid={'transparent'}
                        style={styles.searchInput}
                        value={this.state.githubUsername}
                        onChange={this._onSearchTextChanged}
                        placeholder='myusername'/>
                    <Button
                        //trigger the profile search once the user clicks 'View'
                        onPress={this._handleSearch}
                        color='#48BBEC'
                        title='View'
                    />
                </View>
                //add a nice github image to pretty the UI and a spinner to show when pages are loading
                <Image source={require('./Resources/github.jpeg')} style={styles.image}/>
                {spinner}
            </View>
        );
    }
}

//style sheet for various buttons, views, etc.
const styles = StyleSheet.create({
    description: {
        marginBottom: 20,
        fontSize: 18,
        textAlign: 'center',
        color: '#656565'
    },
    container: {
        padding: 30,
        marginTop: 65,
        alignItems: 'center'
    },
    label: {
        fontSize: 16,
        marginBottom: 6,
    },
    input: {
        width: screenWidth - 20,
        height: 38,
        padding: 4,
        fontSize: 16,
        borderColor: '#3a3a3a',
        borderWidth: 1,
        borderRadius: 8,
    },
    image: {
        width: 217,
        height: 138,
    },
    button: {
        height: 45,
        flexDirection: 'row',
        backgroundColor:'#263238',
        borderColor: '#263238',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        marginTop: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    flowRight: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch',
    },
    searchInput: {
        height: 36,
        padding: 4,
        marginRight: 5,
        flexGrow: 1,
        fontSize: 18,
        borderWidth: 1,
        borderColor: '#48BBEC',
        borderRadius: 8,
        color: '#48BBEC',
    },
    buttonText: {
        color: '#FFFFFF',
        fontSize: 18,
        alignSelf: 'center',
    }
});