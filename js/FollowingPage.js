'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Button,
    ActivityIndicator,
    Image,
} from 'react-native';

type Props = {};
export default class FollowingPage extends Component<Props> {
    static navigationOptions = {
        title: 'Following Page',
    };

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.description}>
                    This is where were gonna list accounts you follow
                </Text>

                <Text style={styles.description}>
                    something
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    description: {
        marginBottom: 20,
        fontSize: 18,
        textAlign: 'center',
        color: '#656565'
    },
    container: {
        padding: 30,
        marginTop: 65,
        alignItems: 'center'
    },
});